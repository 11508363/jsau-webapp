# jsau-webapp
[![pipeline status](https://gitlab.sorbonne-paris-nord.fr/11508363/jsau-webapp/badges/main/pipeline.svg)](https://gitlab.sorbonne-paris-nord.fr/11508363/jsau-webapp/-/commits/main)

[![coverage report](https://gitlab.sorbonne-paris-nord.fr/11508363/jsau-webapp/badges/main/coverage.svg)](https://gitlab.sorbonne-paris-nord.fr/11508363/jsau-webapp/-/commits/main)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
